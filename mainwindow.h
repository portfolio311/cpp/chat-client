#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QDataStream>
#include <QDialog>
#include <QSslSocket>
#include <QNetworkSession>

#include <QLineEdit>
#include <QSpinBox>

#include <QIODevice>

#include <QTcpSocket>

#include <QFile>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QSslSocket *tcpSocket;
    QDataStream mIn;
    QStringList currentData;
    QNetworkSession *networkSession = nullptr;

    QString username;
    QString privateUser;

    QString ver;

    QString dataName;
    qint64 dataSize;
    QString dataUrl;

    QFile data;
    bool ddos;

    QString yourfilename;
    QString yourpath;

    QString fileSender;

    QString reciver;

    QFile file;

private slots:
    void requestData();
    void readServer();
    void displayError(QAbstractSocket::SocketError socketError);
    void on_btn_connect_clicked();
    void sendServer(QStringList listOut);
    void messageHandler(QStringList answer);
    void on_btn_Send_clicked();
    void on_btn_Send_Private_clicked();
    void on_list_user_clicked(const QModelIndex &index);
    void on_btn_discon_clicked();
    void ready();
    void on_btn_Send_File_clicked();
    void sendChunk();
    void on_btn_acceptFile_clicked();
    void on_btn_rejectFile_clicked();
    void on_line_User_returnPressed();
};
#endif // MAINWINDOW_H
