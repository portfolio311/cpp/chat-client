# HTL Chat-Client

Dieser Chat-Client wurde im Ramen des MGIN Unterrichts in der HTBLuVA entwickelt.

Der Server wurde von Dipl.-Ing. (FH) Dr. Andreas Böhler zur Verfügung gestellt und kann [hier](https://github.com/andyboeh/htlChatClient) gefunden werden.