#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QTimer>
#include <QDebug>
#include <QIODevice>
#include <QDataStream>
#include <QStringListModel>
//#include <QMediaPlayer>
#include <QUrl>
#include <QFileDialog>
#include <QTextStream>
#include <QFile>
#include <QFileInfo>
#include <QByteArray>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ver = "Version: 0.2";

    ui->setupUi(this);

    tcpSocket = new QSslSocket(this);

    mIn.setDevice(tcpSocket);
    mIn.setVersion(QDataStream::Qt_5_0);

    connect(tcpSocket, &QIODevice::readyRead, this, &MainWindow::readServer);
    connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),this, &MainWindow::displayError);
    connect(tcpSocket, SIGNAL(encrypted()), this, SLOT(ready()));
    connect(ui->line_Msg, SIGNAL(returnPressed()),ui->btn_Send,SIGNAL(clicked()));

    ui->btn_discon->setVisible(false);
    ui->btn_Send_Private->setVisible(false);
    ui->widget->setVisible(false);
    ui->btn_Send_File->setVisible(false);
    ui->lbl_status->setStyleSheet("background-color:red;");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::messageHandler(QStringList answer){

    ui->textBrowser->setTextColor("Black");

    if(answer[0] == "welcomeToServer"){

        ui->textBrowser->append("Verbindung erfolgreich!\n" + answer[1] + "\n\n");

        if(ver != answer[1]){
            ui->textBrowser->append("Warnung: Client ist auf " + ver + ". Der Server auf " + answer[1] + "\n");
        }

        QStringList out;
        out << "startEncryption";
        sendServer(out);
        qDebug()<<"startEncryption";

        tcpSocket->waitForBytesWritten();
        tcpSocket->startClientEncryption();


    }else if(answer[0] == "commandFailed"){

        QMessageBox::information(this, tr("Error"), tr("Error: %1").arg(answer[1]) + tr(" %1").arg(answer[2]));

    }else if(answer[0] == "commandSuccessful"){

        if(answer[1] == "setUsername"){
            ui->textBrowser->append("Erfolgreich verbunden als: " + ui->line_User->text() + "\n");
            ui->list_user->addItem(username + " (Sie)");

            ui->textBrowser->setTextColor("Green");
            ui->textBrowser->append("\n" + username + " ist beigetreten!\n");
            ui->textBrowser->setTextColor("Black");
        }



    }else if(answer[0] == "newMessage"){

        if(answer[3] == "public"){
            ui->textBrowser->append(answer[1] + ": " + answer[2]);

        }else if(answer[3] == "private"){
            QMessageBox::information(this, tr("Private Nachricht von: %1").arg(answer[1]), tr("%1").arg(answer[1]) + ": " + tr("%1").arg(answer[2]));

            ui->textBrowser->append(answer[1] + " (Privat): " + answer[2]);
        }


    }else if(answer[0] == "newUserConnected"){

        ui->textBrowser->setTextColor("Green");
        ui->textBrowser->append("\n" + answer[1] + " ist beigetreten!\n");
        ui->textBrowser->setTextColor("Black");


        QStringList out;

        out << "getUserList";

        sendServer(out);

    }else if(answer[0] == "userDisconnected"){

        ui->textBrowser->setTextColor("Red");
        ui->textBrowser->append("\n" + answer[1] + " ist gegangen!\n");
        ui->textBrowser->setTextColor("Black");

        QStringList out;

        out << "getUserList";

        sendServer(out);


    }else if(answer[0] == "sendUserList"){

        ui->list_user->clear();

        for(int i = 1; i < answer.length(); i++){

            if(answer[i] == username){
                ui->list_user->addItem(answer[i] + " (Sie)");
            }else{
                ui->list_user->addItem(answer[i]);
            }

        }
    }else if(answer[0] == "startFileTransfer"){

        fileSender = answer[1];

        ui->lbl_recive->setText(answer[1] + " möchte Ihnen die Datei " +  answer[2] + " senden.\nDateigröße: " + answer[3] + " Bytes");
        ui->widget->setVisible(true);
        //ui->textBrowser->append("Benutzername: " + answer[1] + "Dateiname: " + answer[2] + "Dateigröße (Bytes): " + answer[3]);

        yourfilename = answer[2];
    }else if(answer[0] == "finishFileTransfer"){

        //ui->textBrowser->append("Benutzername: " + answer[1]);

        QMessageBox::information(this, tr("HTL Client"), tr("Dateitransfer abgeschlossen."));

    }else if(answer[0] == "sendChunk"){

        QByteArray datachunk = answer[2].toUtf8();
        QByteArray data = QByteArray::fromBase64(datachunk);
        QDir dir; // Initialize to the desired dir if 'path' is relative
        // By default the program's working directory "." is used.

        // We create the directory if needed

        /*if (!dir.exists(yourpath)){
            dir.mkpath(yourpath); // You can check the success if needed
        }*/

        QFile file(yourpath + yourfilename);

        if(file.open(QIODevice::WriteOnly))
        {
            // We're going to streaming text to the file

            /*QTextStream out(&file);
            out<< data;*/

            file.write(data);

            qDebug() << "Writing finished";
        }

        file.close();


    }else if(answer[0] == "fileTransferAccepted"){

        reciver = answer[1];

        QMessageBox::information(this, tr("HTL Client"),
                                 tr("Transfer akzeptiert."));

        sendChunk();

    }else if(answer[0] == "fileTransferRejected"){

        QMessageBox::information(this, tr("HTL Client"),
                                 tr("Transfer abgelehnt."));

    }
}


void MainWindow::sendServer(QStringList listOut){

    QByteArray dat;
    QDataStream mOut(&dat, QIODevice::WriteOnly);

    mOut << listOut;

    tcpSocket->write(dat);

}


void MainWindow::readServer(){
    mIn.startTransaction();

    QStringList nextData;
    mIn >> nextData;

    qDebug() << nextData;

    if(!mIn.commitTransaction()){
        return;
    }

    if(nextData == currentData){
        QTimer::singleShot(0, this, &MainWindow::requestData);
        return;
    }

    currentData = nextData;

    messageHandler(currentData);

    currentData.clear();
    nextData.clear();

    if(tcpSocket->bytesAvailable() > 0) {
        QTimer::singleShot(0, this, SLOT(readServer()));
    }

    ui->btn_connect->setEnabled(true);
}


void MainWindow::displayError(QAbstractSocket::SocketError socketError){
    switch (socketError) {


    case QAbstractSocket::RemoteHostClosedError:
        break;

    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("HTL Client"),
                                 tr("Der Host konnte nicht gedunden werden. "
                                    "Bitte überprüfen Sie den host Namen und den Port."));
        break;

    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("HTL Client"),
                                 tr("Die Verbindung wurde vom Partner verweigert. "
                                    "Bitte überprüfen Sie ob der Server läuft und "
                                    "die Host-Namen und Port Einstellungen korrekt sind"));
        break;

    default:
        QMessageBox::information(this, tr("HTL Client"),
                                 tr("Der folgende Fehler ist aufgetreten: %1.")
                                 .arg(tcpSocket->errorString()));
    }

    ui->btn_connect->setEnabled(true);

}

void MainWindow::requestData(){
    ui->btn_connect->setEnabled(false);
    tcpSocket->abort();
    tcpSocket->connectToHost(ui->line_Host->text(), ui->spin_Port->value());

    tcpSocket->waitForConnected();

    ui->btn_discon->setVisible(true);
    ui->btn_connect->setVisible(false);
    ui->lbl_status->setStyleSheet("background-color:green;");

}

void MainWindow::on_btn_connect_clicked()
{
    if(ui->line_Host->text() == ""){
        QMessageBox::information(this, tr("Error"), tr("Hostename leer"));
    }else if(ui->line_User->text() == ""){
        QMessageBox::information(this, tr("Error"), tr("Username leer"));
    }else{

        username = ui->line_User->text();
        requestData();

    }
}


void MainWindow::on_btn_Send_clicked()
{

    QStringList out;

    out << "sendMessage" << ui->line_Msg->text();

    sendServer(out);

    ui->textBrowser->append(username + " (Sie): " + ui->line_Msg->text());

    ui->line_Msg->clear();


}

void MainWindow::on_btn_Send_Private_clicked()
{

     if(privateUser != username + " (Sie)"){

        QStringList out;

        out << "sendMessage" << ui->line_Msg->text() << privateUser;

        ui->textBrowser->append(username + " (Privat): "  + ui->line_Msg->text());

        sendServer(out);

        ui->line_Msg->clear();

    }else{
        QMessageBox::information(this, tr("Keine Freunde"), tr("Error 404:\nKeine Freunde gefunden!"));
    }

}


void MainWindow::on_list_user_clicked(const QModelIndex &index)
{
    privateUser = ui->list_user->item(index.row())->text();

    ui->btn_Send_Private->setVisible(true);
    ui->btn_Send_File->setVisible(true);
}

void MainWindow::on_btn_discon_clicked()
{
    tcpSocket->disconnectFromHost();

    ui->btn_connect->setVisible(true);
    ui->btn_discon->setVisible(false);

    ui->textBrowser->clear();
    ui->list_user->clear();

    ui->lbl_status->setStyleSheet("background-color:red;");
}

void MainWindow::ready()
{
    qDebug() << "Verbindung gesichert.";


    QStringList out;

    out << "setUsername" << ui->line_User->text();

    sendServer(out);


    out.clear();
    out << "getUserList";
    sendServer(out);

}

void MainWindow::on_btn_Send_File_clicked(){

    dataUrl = QFileDialog::getOpenFileName(this, "Send File ...");
    QStringList fileName = dataUrl.split('/');

    qDebug() << dataUrl;
    qDebug() << fileName;
    qDebug() << fileName[fileName.length() - 1];

    dataName = fileName[fileName.length() - 1];

    QFileInfo fd = dataUrl;
    dataSize = fd.size();

    qDebug() << dataSize;

    QString fn = fd.fileName();

    QStringList out;
    out << "startFileTransfer" << privateUser << fn << QString::number(dataSize);
    sendServer(out);
}

void MainWindow::sendChunk(){


    QFile file(dataUrl); // this is a name of a file text1.txt sent from main method

    file.open(QIODevice::ReadOnly);
    QByteArray fn = file.readAll();

    QString encoded = QString(fn.toBase64());

    qDebug()<<encoded;

    QStringList out;
    out<<"sendChunk"<<reciver<<encoded;
    sendServer(out);

    out.clear();
    out<<"finishFileTransfer"<<reciver;
    sendServer(out);

}

void MainWindow::on_btn_acceptFile_clicked()
{
    yourpath= QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home");

    QStringList out;
    out<<"acceptFileTransfer"<<fileSender;
    sendServer(out);

    yourpath.append('/');
    qDebug()<<yourpath;

    ui->widget->setVisible(false);
}

void MainWindow::on_btn_rejectFile_clicked()
{
    QStringList out;
    out<<"rejectFileTransfer"<<fileSender;
    sendServer(out);

    ui->widget->setVisible(false);
}

void MainWindow::on_line_User_returnPressed()
{
    on_btn_connect_clicked();
}
